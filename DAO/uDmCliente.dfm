object DmCliente: TDmCliente
  OldCreateOrder = False
  Height = 267
  Width = 449
  object sqlPesquisar: TSQLDataSet
    CommandText = 
      'SELECT C.ID, C.NOME, C.TELEFONE, CID.NOME AS CIDADE'#13#10'FROM CLIENT' +
      'E as C'#13#10'LEFT JOIN CIDADE as CID ON (C.ID_CIDADE = CID.ID)'#13#10'WHERE' +
      ' C.NOME LIKE :NOME'
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftString
        Name = 'nome'
        ParamType = ptInput
      end>
    SQLConnection = DmConexao.sqlConexao
    Left = 64
    Top = 32
  end
  object sqlInserir: TSQLDataSet
    CommandText = 
      'INSERT INTO CLIENTE (ID, ID_CIDADE, NOME, CPF, RG, TELEFONE)'#13#10'VA' +
      'LUES (:ID, ID_CIDADE, :NOME, :CPF, :RG, :TELEFONE)'
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NOME'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'CPF'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'RG'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TELEFONE'
        ParamType = ptInput
      end>
    SQLConnection = DmConexao.sqlConexao
    Left = 136
    Top = 32
  end
  object sqlAlterar: TSQLDataSet
    CommandText = 
      'UPDATE CLIENTE'#13#10'SET ID_CIDADE = :ID_CIDADE,'#13#10'    NOME = :NOME,'#13#10 +
      '    CPF = :CPF,'#13#10'    RG = :RG,'#13#10'    TELEFONE = :TELEFONE,'#13#10'WHERE' +
      ' (ID = :ID)'
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CIDADE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NOME'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'CPF'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'RG'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TELEFONE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    SQLConnection = DmConexao.sqlConexao
    Left = 208
    Top = 32
  end
  object sqlExcluir: TSQLDataSet
    CommandText = 'DELETE FROM CLIENTE'#13#10'WHERE (ID = :ID)'
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    SQLConnection = DmConexao.sqlConexao
    Left = 280
    Top = 32
  end
  object dspPesquisar: TDataSetProvider
    DataSet = sqlPesquisar
    Left = 64
    Top = 96
  end
  object cdsPesquisar: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'nome'
        ParamType = ptInput
      end>
    ProviderName = 'dspPesquisar'
    Left = 64
    Top = 160
    object cdsPesquisarID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsPesquisarNOME: TStringField
      FieldName = 'NOME'
      Required = True
      Size = 80
    end
    object cdsPesquisarTELEFONE: TStringField
      FieldName = 'TELEFONE'
      Required = True
    end
    object cdsPesquisarCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 80
    end
  end
end
