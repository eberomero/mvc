unit uDmCliente;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Data.DB, Datasnap.DBClient,
  Datasnap.Provider, Data.SqlExpr, Vcl.Grids, uClienteModel;

type
  TDmCliente = class(TDataModule)
    sqlPesquisar: TSQLDataSet;
    sqlInserir: TSQLDataSet;
    sqlAlterar: TSQLDataSet;
    sqlExcluir: TSQLDataSet;
    dspPesquisar: TDataSetProvider;
    cdsPesquisar: TClientDataSet;
    cdsPesquisarID: TIntegerField;
    cdsPesquisarNOME: TStringField;
    cdsPesquisarTELEFONE: TStringField;
    cdsPesquisarCIDADE: TStringField;
  private
    { Private declarations }
  public
    procedure Pesquisar(Nome: String; var StringGrid: TStringGrid);
    procedure CarregarCliente(oCliente: TCliente; Codigo: Integer);
    function Inserir(oCliente: TCliente;out Erro: String): Boolean;
    function Alterar(oCliente: TCliente;out Erro: String): Boolean;
    function Excluir(Cliente: Integer;out Erro: String): Boolean;
  end;

var
  DmCliente: TDmCliente;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDmConexao;

{$R *.dfm}

{ TDmCliente }

function TDmCliente.Alterar(oCliente: TCliente; out Erro: String): Boolean;
begin
  if oCliente.ID_CIDADE = 0 then
    sqlAlterar.ParamByName('ID_CIDADE').Clear
  else
    sqlAlterar.ParamByName('ID_CIDADE').AsInteger := oCliente.ID_CIDADE;
  sqlAlterar.ParamByName('NOME').AsString     := oCliente.Nome;
  sqlAlterar.ParamByName('CPF').AsString      := oCliente.CPF;
  sqlAlterar.ParamByName('RG').AsString       := oCliente.RG;
  sqlAlterar.ParamByName('TELEFONE').AsString := oCliente.Telefone;
  sqlAlterar.ParamByName('ID').AsInteger      := oCliente.ID;
  try
    sqlAlterar.ExecSQL();
    Result := True;
  except
    on E: Exception do
    begin
      Erro   := Format('Ocorreu um erro ao alterar o cliente: %s - %s',[sLineBreak, E.Message]);
      Result := False;
    end;
  end;
end;

procedure TDmCliente.CarregarCliente(oCliente: TCliente; Codigo: Integer);
var
  sqlSequencia: TSQLDataSet;
  SQL         : TStringList;
begin
  sqlSequencia := TSQLDataSet.Create(nil);
  SQL          := TStringList.Create;
  try
    SQL.Add('SELECT C.*, CID.NOME AS CIDADE, E.SIGLA');
    SQL.Add('FROM CLIENTE C');
    SQL.Add('LEFT JOIN CIDADE CID ON CID.ID = C.ID_CIDADE');
    SQL.Add('LEFT JOIN ESTADO E   ON E.ID = CID.ID_ESTADO');
    SQL.Add(Format('WHERE C.ID = %d',[Codigo]));

    sqlSequencia.SQLConnection := DmConexao.sqlConexao;
    sqlSequencia.CommandText   := SQL.Text;
    sqlSequencia.Open;

    oCliente.ID        := sqlSequencia.FieldByName('ID').AsInteger;
    oCliente.ID_CIDADE := sqlSequencia.FieldByName('ID_CIDADE').AsInteger;
    oCliente.RG        := sqlSequencia.FieldByName('RG').AsString;
    oCliente.CPF       := sqlSequencia.FieldByName('CPF').AsString;
    oCliente.Nome      := sqlSequencia.FieldByName('NOME').AsString;
    oCliente.Telefone  := sqlSequencia.FieldByName('TELEFONE').AsString;
    oCliente.Cidade    := sqlSequencia.FieldByName('CIDADE').AsString;
    oCliente.Estado    := sqlSequencia.FieldByName('SIGLA').AsString;
  finally
    FreeAndNil(sqlSequencia);
    FreeAndNil(SQL);
  end;
end;

function TDmCliente.Excluir(Cliente: Integer; out Erro: String): Boolean;
begin
  sqlExcluir.ParamByName('ID').AsInteger := Cliente;
  try
    sqlExcluir.ExecSQL();
    Result := True;
  except
    on E: Exception do
    begin
      Erro   := Format('Ocorreu um erro ao excluir o cliente: %s - %s',[sLineBreak, E.Message]);
      Result := False;
    end;
  end;
end;

function TDmCliente.Inserir(oCliente: TCliente; out Erro: String): Boolean;
begin
  sqlInserir.ParamByName('ID').AsInteger := DmConexao.GerarID('ID', 'CLIENTE');
  if oCliente.ID_CIDADE = 0 then
    sqlInserir.ParamByName('ID_CIDADE').Clear
  else
    sqlInserir.ParamByName('ID_CIDADE').AsInteger := oCliente.ID_CIDADE;
  sqlInserir.ParamByName('NOME').AsString     := oCliente.Nome;
  sqlInserir.ParamByName('CPF').AsString      := oCliente.CPF;
  sqlInserir.ParamByName('RG').AsString       := oCliente.RG;
  sqlInserir.ParamByName('TELEFONE').AsString := oCliente.Telefone;
  try
    sqlInserir.ExecSQL();
    Result := True;
  except
    on E: Exception do
    begin
      Erro   := Format('Ocorreu um erro ao inserir o cliente: %s - %s',[sLineBreak, E.Message]);
      Result := False;
    end;
  end;
end;

procedure TDmCliente.Pesquisar(Nome: String; var StringGrid: TStringGrid);
var
  Total: Integer;
begin
  try
    Total := 1;
    if cdsPesquisar.Active then
      cdsPesquisar.Close;

    if Nome.IsEmpty then
      cdsPesquisar.Params[0].AsString := '%'
    else
      cdsPesquisar.Params[0].AsString := Format('%s%s%s',['%',Nome,'%']);

    cdsPesquisar.Open;
    cdsPesquisar.DisableControls;
    cdsPesquisar.Last;

    StringGrid.RowCount := cdsPesquisar.RecordCount;
    StringGrid.ColCount := cdsPesquisar.FieldCount;
    StringGrid.Cells[0,0] := 'C�digo';
    StringGrid.Cells[1,0] := 'Nome';
    StringGrid.Cells[2,0] := 'Telefone';
    StringGrid.Cells[3,0] := 'Cidade';
    StringGrid.ColWidths[0] := 40;
    StringGrid.ColWidths[1] := 200;
    StringGrid.ColWidths[2] := 120;
    StringGrid.ColWidths[3] := 180;
    cdsPesquisar.First;
    while not cdsPesquisar.eof do
    begin
      StringGrid.Cells[0, Total]  := cdsPesquisar.FieldByName('ID').AsString;
      StringGrid.Cells[1, Total]  := cdsPesquisar.FieldByName('NOME').AsString;
      StringGrid.Cells[2, Total]  := cdsPesquisar.FieldByName('TELEFONE').AsString;
      StringGrid.Cells[3, Total]  := cdsPesquisar.FieldByName('CIDADE').AsString;
      inc(Total);
      cdsPesquisar.Next;
    end;
  finally
    cdsPesquisar.EnableControls;
    cdsPesquisar.Close;
  end;
end;

end.
