unit uClienteModel;

interface

uses
  System.SysUtils;

type
  TCliente = class
  private
    FID:        Integer;
    FID_CIDADE: integer;
    FCidade: String;
    FEstado: String;
    FTelefone: String;
    FRG: String;
    FCPF: String;
    FNome: String;
    procedure SetNome(const Value: String);
    procedure SetTelefone(const Value: String);
  public

    property ID:        Integer read FID        write FID;
    property ID_CIDADE: Integer read FID_CIDADE write FID_CIDADE;
    property RG:        String  read FRG        write FRG;
    property Nome:      String  read FNome      write FNome;
    property Telefone:  String  read FTelefone  write FTelefone;
    property Cidade:    String  read FCidade    write FCidade;
    property Estado:    String  read FEstado    write FEstado;
    property CPF:       String  read FCPF       write FCPF;

  end;

implementation

{ TCliente }


{ TCliente }

procedure TCliente.SetNome(const Value: String);
begin
  if Value = EmptyStr then
    raise EArgumentException.Create('O ''Nome'' precisa ser preenchido')
  else
    FNome := Value;
end;

procedure TCliente.SetTelefone(const Value: String);
begin
  if Value = EmptyStr then
    raise EArgumentException.Create('O ''Telefone'' precisa ser preenchido');
  FTelefone := Value;
end;

end.
