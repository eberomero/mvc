program MVCDAO;

uses
  Vcl.Forms,
  uFrmPrincipal in 'VIEW\uFrmPrincipal.pas' {FrmPrincipal},
  uClienteModel in 'MODEL\uClienteModel.pas',
  uFrmCadastrarCliente in 'VIEW\uFrmCadastrarCliente.pas' {FrmCadastrarCliente},
  uDmConexao in 'DAO\uDmConexao.pas' {DmConexao: TDataModule},
  uDmCliente in 'DAO\uDmCliente.pas' {DmCliente: TDataModule},
  uClienteController in 'CONTROLLER\uClienteController.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.CreateForm(TDmConexao, DmConexao);
  Application.Run;
end.
