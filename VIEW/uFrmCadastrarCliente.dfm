object FrmCadastrarCliente: TFrmCadastrarCliente
  Left = 0
  Top = 0
  Caption = 'Cadastrar Cliente'
  ClientHeight = 397
  ClientWidth = 596
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnlRodape: TPanel
    Left = 0
    Top = 364
    Width = 596
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    Color = 11391740
    ParentBackground = False
    TabOrder = 0
    ExplicitTop = 365
    object btnFechar: TButton
      Left = 504
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Fechar'
      TabOrder = 0
    end
  end
  object pgcPrincipal: TPageControl
    Left = 0
    Top = 0
    Width = 596
    Height = 364
    ActivePage = tbPesq
    Align = alClient
    TabOrder = 1
    ExplicitHeight = 356
    object tbPesq: TTabSheet
      Caption = 'tbPesq'
      ExplicitLeft = 8
      ExplicitTop = 26
      object pnlFiltro: TPanel
        Left = 0
        Top = 0
        Width = 588
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        Color = 11391740
        ParentBackground = False
        TabOrder = 0
        object edtPesq: TLabeledEdit
          Left = 8
          Top = 21
          Width = 479
          Height = 21
          EditLabel.Width = 101
          EditLabel.Height = 13
          EditLabel.Caption = 'Digite para pesquisar'
          TabOrder = 0
        end
        object btnPesquisar: TButton
          Left = 500
          Top = 17
          Width = 75
          Height = 25
          Caption = 'Pesquisar'
          TabOrder = 1
        end
      end
      object pnlPesqBtns: TPanel
        Left = 0
        Top = 295
        Width = 588
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        Color = 11391740
        ParentBackground = False
        TabOrder = 1
        ExplicitTop = 285
        object btnNovo: TButton
          Left = 324
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Novo'
          TabOrder = 0
        end
        object btnDetalhar: TButton
          Left = 412
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Detalhar'
          TabOrder = 1
        end
        object btnExcluir: TButton
          Left = 500
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Excluir'
          TabOrder = 2
        end
      end
      object grdPesquisa: TStringGrid
        Left = 0
        Top = 48
        Width = 588
        Height = 247
        Align = alClient
        FixedCols = 0
        TabOrder = 2
        ExplicitLeft = 56
        ExplicitTop = 104
        ExplicitWidth = 320
        ExplicitHeight = 120
      end
    end
    object tbDados: TTabSheet
      Caption = 'tbDados'
      ImageIndex = 1
      ExplicitLeft = 8
      ExplicitTop = 26
      object edCodigo: TLabeledEdit
        Left = 3
        Top = 24
        Width = 40
        Height = 21
        EditLabel.Width = 33
        EditLabel.Height = 13
        EditLabel.Caption = 'C'#243'digo'
        Enabled = False
        TabOrder = 0
      end
      object edtNome: TLabeledEdit
        Left = 3
        Top = 80
        Width = 341
        Height = 21
        EditLabel.Width = 27
        EditLabel.Height = 13
        EditLabel.Caption = 'Nome'
        TabOrder = 1
      end
      object edtRG: TLabeledEdit
        Left = 348
        Top = 80
        Width = 107
        Height = 21
        EditLabel.Width = 14
        EditLabel.Height = 13
        EditLabel.Caption = 'RG'
        TabOrder = 2
      end
      object edtCPF: TLabeledEdit
        Left = 459
        Top = 80
        Width = 107
        Height = 21
        EditLabel.Width = 19
        EditLabel.Height = 13
        EditLabel.Caption = 'CPF'
        TabOrder = 3
      end
      object edtCodCidade: TLabeledEdit
        Left = 3
        Top = 128
        Width = 63
        Height = 21
        EditLabel.Width = 59
        EditLabel.Height = 13
        EditLabel.Caption = 'C'#243'd. Cidade'
        TabOrder = 4
      end
      object edNomeCidade: TLabeledEdit
        Left = 70
        Top = 128
        Width = 458
        Height = 21
        EditLabel.Width = 33
        EditLabel.Height = 13
        EditLabel.Caption = 'Cidade'
        Enabled = False
        TabOrder = 5
      end
      object edtUF: TLabeledEdit
        Left = 532
        Top = 128
        Width = 34
        Height = 21
        EditLabel.Width = 13
        EditLabel.Height = 13
        EditLabel.Caption = 'UF'
        Enabled = False
        TabOrder = 6
      end
      object edtTelefone: TLabeledEdit
        Left = 3
        Top = 176
        Width = 182
        Height = 21
        EditLabel.Width = 42
        EditLabel.Height = 13
        EditLabel.Caption = 'Telefone'
        TabOrder = 7
      end
      object pnlCadBtns: TPanel
        Left = 0
        Top = 303
        Width = 588
        Height = 33
        Align = alBottom
        BevelOuter = bvNone
        Color = 11391740
        ParentBackground = False
        TabOrder = 8
        ExplicitTop = 365
        ExplicitWidth = 596
        object btnListar: TButton
          Left = 228
          Top = 5
          Width = 75
          Height = 25
          Caption = 'Listar'
          TabOrder = 0
        end
        object btnAlterar: TButton
          Left = 332
          Top = 5
          Width = 75
          Height = 25
          Caption = 'Alterar'
          TabOrder = 1
        end
        object btnGravar: TButton
          Left = 416
          Top = 5
          Width = 75
          Height = 25
          Caption = 'Gravar'
          TabOrder = 2
        end
        object btnCancelar: TButton
          Left = 500
          Top = 5
          Width = 75
          Height = 25
          Caption = 'Cancelar'
          TabOrder = 3
        end
      end
    end
  end
end
