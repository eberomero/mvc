unit uFrmCadastrarCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.Grids;

type
  TFrmCadastrarCliente = class(TForm)
    pnlRodape: TPanel;
    pgcPrincipal: TPageControl;
    btnFechar: TButton;
    tbPesq: TTabSheet;
    tbDados: TTabSheet;
    pnlFiltro: TPanel;
    pnlPesqBtns: TPanel;
    edtPesq: TLabeledEdit;
    btnPesquisar: TButton;
    btnNovo: TButton;
    btnDetalhar: TButton;
    btnExcluir: TButton;
    edCodigo: TLabeledEdit;
    edtNome: TLabeledEdit;
    edtRG: TLabeledEdit;
    edtCPF: TLabeledEdit;
    edtCodCidade: TLabeledEdit;
    edNomeCidade: TLabeledEdit;
    edtUF: TLabeledEdit;
    edtTelefone: TLabeledEdit;
    pnlCadBtns: TPanel;
    btnListar: TButton;
    btnAlterar: TButton;
    btnGravar: TButton;
    btnCancelar: TButton;
    grdPesquisa: TStringGrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCadastrarCliente: TFrmCadastrarCliente;

implementation

{$R *.dfm}

end.
